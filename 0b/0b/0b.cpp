#include "pch.h"
using namespace std;

string get_word(string* str)
{
	int i;
	string word;
	for (i = 0; isalpha((*str)[i]) || isdigit((*str)[i]); i++);
	word.assign(*str, 0, i);
	(*str).erase(0, i+1);
	return word;
}

bool pred(pair< string, int > &a, pair< string, int > &b)
{
	return a.second < b.second;
}

int main(int argc, char**argv)
{
	ifstream in;
	ofstream out;
	string str;
	int count = 0;
	map <string, int> gist;
	in.open(argv[1]);
	out.open(argv[2]);
	if (!in.is_open())
	{
		cout << "ошибка чтения файла" << endl;
	}
	if (!out.is_open())
	{
		cout << "ошибка записи файла" << endl;
	}
	while (!in.eof())
	{
		getline(in, str);
		while (!str.empty())
		{
			string word = get_word(&str);
			if (!word.empty())
			{
				gist[word] = gist[word] + 1;
				count++;
			}
		}

	}
	vector<pair< string, int >> vgist(gist.begin(), gist.end());
	sort(vgist.begin(), vgist.end(), pred);
	in.close();
	for (vector<pair< string, int >>::iterator i = vgist.begin(); i != vgist.end(); i++)
	{
		out << (*i).first << ";" << (*i).second << ";" << (*i).second*100 / count << "%"  << endl;
	}
	out.close();
}